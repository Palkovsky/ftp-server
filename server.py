import socket
import os
import time
from threading import *
from utils import *

dtp = makeTCPSocket(('localhost', 20))
def send_dtp(self, b):
    dtp.sendall(b)
    dtp.close()

class FTPSessionThread(Thread):
    def __init__(self, s, dir = "/"):
        Thread.__init__(self)
        self.conn = s

        self.basePath = dir
        self.path = dir
        self.username = "anonymous"
        self.transfer_type = "I"
        self.running = True

    def handle_command(self, cmd, arg):
        try:
            getattr(self, '_' + cmd.upper() + '_cmd')(arg)
        except AttributeError:
            self.reply('Unsupported command: ' + cmd + ' ' + arg, code = 502)

    def send_command(self, command, arg):
        self.conn.sendall(makeCommand(command, str(arg)))

    def reply(self, text, code = 200):
        s = str(code) + ' ' + str(text) + '\r\n'
        self.conn.sendall(s.encode('ascii'))

    def _PORT_cmd(self, arg):
        dtp.close()
        addr, port = decodeIp4(arg)
        print("DTP >>", "Changing user DTP to:", (addr, port))
        dtp = makeTCPSocket(('localhost', 20))
        dtp.connect((addr, port))
        self.reply('Connected.')

    def _TYPE_cmd(self, arg):
        self.transfer_type = arg
        self.reply('Type changed.')

    def _USER_cmd(self, arg):
        self.username = arg
        self.reply('Hello, ' + self.username + '!', code = 230)

    def _NLST_cmd(self, arg):
        self._LIST_cmd(arg)

    def _LIST_cmd(self, arg):
        arg = '.' if arg == '' else arg
        p = os.path.join(self.path, arg)

        if os.path.isdir(p):
            dirs = os.listdir(p)
            self.reply('Listing ' + p, code = 125)
            send_dtp(b'\r\n'.join([dir.encode('ascii') for dir in dirs]) + b'\r\n')
            self.reply('Listing sent.', code = 250)
        elif os.path.isfile(p):
            pass
        else:
            self.reply('Unexistent path.', code = 450)


    def _CWD_cmd(self, arg):
        self.path = os.path.join(self.path, arg) # absolutely insecure
        self.reply('New working directory: ' + self.path)

    def _STOR_cmd(self, arg):
        new_path = os.path.join(self.path, '.' if arg == '' else arg)
        self.reply('Listing ' + p, code = 125)
        dtp.listen()
        conn, _ = dtp.accept()
        bytes = recv_all(conn)
        with open(new_path, 'wb') as f:
            f.write(bytes)
        conn.close()
        dtp.close()
        self.reply('File received sent.', code = 250)


    def _XPWD_cmd(self, arg):
        self._PWD_cmd(arg)

    def _PWD_cmd(self, arg):
        self.reply(self.path, code = 257)

    def _QUIT_cmd(self, arg):
        self.running = False
        self.reply('Bye, bye!', code = 221)

    def run(self):
        try:
            self.reply("HELLO WORLD", code = 220)
            while self.running:
                data = self.conn.recv(1024)
                cmd, arg = parseCommand(data)
                if not cmd:
                    continue
                print("PI >>", cmd, arg)
                self.handle_command(cmd, arg)
        except Exception as e:
            print(str(e), flush=True)
        finally:
            dtp.close()
            self.conn.close()

with makeTCPSocket(('localhost', 21)) as ftp: #server PI
    ftp.listen(1)
    while True:
        print("Started FTP server on ", ftp.getsockname())
        print("Listening...")
        pi, addr = ftp.accept()
        print('Connected by ', addr)
        ftpThread = FTPSessionThread(pi, os.path.join('.', 'ftp_data'))
        ftpThread.start()
        ftpThread.join()
