import socket

def makeTCPSocket(addr):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(addr)
    return s

def makeCommand(name, arg = ''):
    b = bytearray()
    b.extend(map(ord, ''.join([name, '' if arg == '' else ' ', arg, '\r\n'])))
    return b

def parseCommand(cmd):
    chunks = cmd.decode("ascii") .strip('\r\n').split(' ')
    if not chunks:
        raise ValueError("Empty command.")
    return (chunks[0], '') if len(chunks) == 1 else (chunks[0], chunks[1])

def printHex(s):
    print(":".join("{:02x}".format(ord(c)) for c in s))

def recv_all(sock, buff_size=1024):
    data = b''
    while True:
        packet = sock.recv(buff_size)
        if not packet:
            return None
        data += packet
    return data

def encodeIp4(addr, port):
    addr = '127.0.0.1' if 'localhost' else addr
    res = ','.join(addr.split('.')) + ','

    portStr = str(port)
    while portStr:
        if len(portStr) == 1:
            res += portStr[0] + ','
            portStr = None
        else:
            res += portStr[:2] + ','
            portStr = portStr[2:]

    return res[:-1]

def decodeIp4(s):
    chunks = s.split(',')
    addr = '.'.join(chunks[:4])
    port = '0x' + ''.join([hex(int(c)).split('x')[-1] for c in chunks[4:]])
    return (addr, int(port, 16))
