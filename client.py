import socket
from utils import *

def command(s, c, a=''):
    s.sendall(makeCommand(c, a))
    res = s.recv(1024).decode('ascii')
    status, res = tuple(res.split(' ', 1))
    data = None

    if status in [125, 150]: # now open use dtp
        data = "tududududu" # read data

        r = s.recv(1024).decode('ascii')
        code, _ = tuple(res.split(' ', 1))
        status = code

        if code in [450, 451, 425]:
            data = None
            return (int(status), res, data)

        if code in [226, 250]:
            return (int(status), res, data)

    return (int(status), res, data)

# TO DO: interactivity

sControl = makeTCPSocket(('localhost', 0))
sData = makeTCPSocket(('localhost', sControl.getsockname()[1] + 1))
sData.listen()

sControl.settimeout(1.0)
sControl.connect(('localhost', 21))

print(command(sControl, 'PORT', encodeIp4(*(sData.getsockname()))))

dtp, _ = sData.accept()

print(command(sControl, 'USER', 'dawid'))
print(command(sControl, 'PWD'))

sControl.close()
sData.close()
